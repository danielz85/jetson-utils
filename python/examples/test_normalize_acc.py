from time import time
import jetson.utils
import numpy as np

WIDTH, HEIGHT, DEPTH = 1920, 1080, 3
array = np.random.rand(HEIGHT, WIDTH, DEPTH).astype(np.float32) * 255.0
print('numpy array shape:  ' + str(array.shape))
print('numpy array dtype:  ' + str(array.dtype))

cuda_mem = jetson.utils.cudaFromNumpy(array)
result_buffer = jetson.utils.cudaAllocMapped(width=array.shape[1], height=array.shape[0], format='rgb32f')

trials = 50

t0 = time()
for i in range(trials):
    res0 = array / 255.0
tdiff0 = time() - t0

t0 = time()
for i in range(trials):
    jetson.utils.cudaNormalize(cuda_mem, (0., 255.0), result_buffer, (0., 1.))
tdiff1 = time() - t0
res1 = jetson.utils.cudaToNumpy(result_buffer)

print("no GPU: %f" % tdiff0)
print("with GPU: %f" % tdiff1)
print("acceleration: %d" % (tdiff0 / tdiff1))

print("testing results are equal...")
assert np.max(res1 - res0) < 10**-7, "results differ"
print("done")
